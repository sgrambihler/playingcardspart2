
// Playing Cards
// Paul Haller
// Sawyer Grambihler

#include <iostream>
#include <conio.h>

using namespace std;

string suitString[4] = {"Spades", "Diamonds", "Hearts", "Clubs"};
string rankString[13] = {"Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King", "Ace"};

enum class Suit
{
	SPADES,
	DIAMONDS,
	HEARTS,
	CLUBS
};

enum class Rank
{
	TWO,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE
};

struct Card
{
	Rank Rank;
	Suit Suit;
};

void PrintCard(Card card);
Card HighCard(Card card1, Card card2);

int main()
{
	Card c1;
	c1.Rank = Rank::ACE;
	c1.Suit = Suit::DIAMONDS;

	Card c2;
	c2.Rank = Rank::NINE;
	c2.Suit = Suit::SPADES;

	PrintCard(HighCard(c2, c1));

	(void)_getch();
	return 0;
}

void PrintCard(Card card) {
	cout << "The " << rankString[(int)card.Rank] << " of " << suitString[(int)card.Suit];
}

Card HighCard(Card card1, Card card2) {
	if ((int)card1.Rank > (int)card2.Rank)
	{
		return card1;
	}
	else {
		return card2;
	}
}